﻿using HostedServiceDemo.EfContext;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Quartz;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace BackgroundServiceDemo
{
    public class TestJob : IJob
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _config;
        private readonly EFContext _context;
        private readonly IServiceScopeFactory _scopeFactory;

        public TestJob(ILogger<TestJob> logger, IConfiguration config, IServiceScopeFactory scopeFactory)
        {
            _logger = logger;
            _config = config;
            _scopeFactory = scopeFactory;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            _logger.LogInformation(string.Format("[{0:yyyy-MM-dd hh:mm:ss:ffffff}]任务执行！", DateTime.Now));

            // 读取配置
            Console.WriteLine($"读取配置{ _config["test"] }");

            using (var scope = _scopeFactory.CreateScope())
            {
                var _context = scope.ServiceProvider.GetRequiredService<EFContext>();
                // 读取数据库
                var model = await _context.QuartInfo.FirstOrDefaultAsync();
                if (model != null)
                {
                    Console.WriteLine($"读取数据库 { model.jobName }");
                }
            }
        }
    }
}
