﻿using HostedServiceDemo.EfContext;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Quartz;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HostedServiceDemo
{
    public class TestJob : IJob
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _config;
        private readonly EFContext _context;

        public TestJob(ILogger<TestJob> logger, IConfiguration config, EFContext context)
        {
            _logger = logger;
            _config = config;
            _context = context;

        }

        public async Task Execute(IJobExecutionContext context)
        {
            _logger.LogInformation(string.Format("[{0:yyyy-MM-dd hh:mm:ss:ffffff}]任务执行！", DateTime.Now));

            // 读取配置
            Console.WriteLine($"读取配置{ _config["test"] }");

            // 读取数据库
            var model = await _context.QuartInfo.FirstOrDefaultAsync();
            if (model != null)
            {
                Console.WriteLine($"读取数据库 { model.jobName }");
            }
        }
    }
}
