﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HostedServiceDemo.Model
{
    public class QuartzInfo
    {
        public string guid { get; set; }
        public string triggerGroup { get; set; }
        public string triggerName { get; set; }
        public string cronExpression { get; set; }
        public string fullClassName { get; set; }
        public string jobGroup { get; set; }
        public string jobName { get; set; }
    }
}
