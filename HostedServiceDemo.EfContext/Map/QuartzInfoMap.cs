﻿using HostedServiceDemo.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace HostedServiceDemo.EfContext.Map
{
    public class QuartzInfoMap : IEntityTypeConfiguration<QuartzInfo>
    {
        public void Configure(EntityTypeBuilder<QuartzInfo> builder)
        {
            //表名
            builder.ToTable("QuartzInfo");

            //主键
            builder.HasKey(x => x.guid);

            //字段
            builder.Property(x => x.triggerGroup).HasColumnType("nvarchar(50)");
            builder.Property(x => x.triggerName).HasColumnType("nvarchar(50)");
            builder.Property(x => x.cronExpression).HasColumnType("nvarchar(200)");
            builder.Property(x => x.fullClassName).HasColumnType("nvarchar(200)");
            builder.Property(x => x.jobGroup).HasColumnType("nvarchar(50)");
            builder.Property(x => x.jobName).HasColumnType("nvarchar(50)");

            //索引
        }
    }
}
